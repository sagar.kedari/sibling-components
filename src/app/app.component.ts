import { Component, Input } from '@angular/core';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  numInCard1=0
  numInCard2=0

  changeValue1(numInCard1:number) {
    this.numInCard1 = numInCard1
  }
  changeValue2(numInCard2:number) {
    this.numInCard2 = numInCard2
  }
  // initialCountCard1 = 0;
  // initialCountCard2 = 0;
  
}
