import { Component, Input, OnInit, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-card1',
  templateUrl: './card1.component.html',
  styleUrls: ['./card1.component.scss']
})
export class Card1Component  {
  @Input() numInCard1 = 0
  numInCard2 = 0
  @Output() emit1 = new EventEmitter()
  increment(){
    this.numInCard2++
    this.emit1.emit(this.numInCard2)
  }

}
