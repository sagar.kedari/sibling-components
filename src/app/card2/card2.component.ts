import { Component, Input, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-card2',
  templateUrl: './card2.component.html',
  styleUrls: ['./card2.component.scss']
})
export class Card2Component  {
  @Input() numInCard2 = 0
  numInCard1 = 0
  @Output() emit2 = new EventEmitter();
  increment2(){
    this.numInCard1++;
    this.emit2.emit(this.numInCard1)
  }

}
